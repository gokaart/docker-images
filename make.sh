#!/usr/bin/env bash
set -ex
REGISTRY="registry.gitlab.com/gokaart/docker-images"
BUILDX_CACHE=".cache"
DOCKER="docker buildx build --platform linux/arm64,linux/amd64 --pull --push --cache-to type=local,dest=${BUILDX_CACHE} --cache-from type=local,src=${BUILDX_CACHE}"

IMAGE="${REGISTRY}/postgis:latest"
${DOCKER} --tag ${IMAGE} --cache-from ${IMAGE} --cache-from postgis/postgis:latest --cache-from postgis/postgis:13-3.1 --tag ${REGISTRY}/postgis:13-3.1 postgis/13-3.1/

#IMAGE="${REGISTRY}/postgis:latest-alpine"
#${DOCKER} --tag ${IMAGE} --cache-from ${IMAGE} --cache-from postgis/postgis:13-3.1-alpine --tag ${REGISTRY}/postgis:13-3.1-alpine postgis/13-3.1/alpine

DOCKER="docker buildx build --platform linux/arm64,linux/amd64 --pull --push"

IMAGE="${REGISTRY}/node:latest"
${DOCKER} --tag ${IMAGE} --cache-from ${IMAGE} node

IMAGE="${REGISTRY}/gdal-python-git-postgres:latest"
${DOCKER} --tag ${IMAGE} --cache-from ${IMAGE} gdal-python
